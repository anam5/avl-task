import subprocess

def deploy_microservices():
    try:
        subprocess.run(["docker-compose", "-f", "docker-compose.local.yml", "up", "-d"], check=True)
        print(f"Microservices deployed successfully!")

    except Exception as e:
        print(f"Error: {e}")

if __name__ == "__main__":
    deploy_microservices()
    