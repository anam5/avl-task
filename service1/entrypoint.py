import hashlib
from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/', methods=['POST'])
def calculate_hash():

    data = request.json  

    if data is None or 'hash_func' not in data or 'message' not in data:
        return jsonify({'error': 'Invalid data'}), 400
    
    hash_func = data.get('hash_func')
    message = data.get('message')

    h = hashlib.new(hash_func)
    h.update(str.encode(message))
    
    result = {'hash': h.hexdigest()}
    return jsonify(result)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8081)



