import requests

SERVICE1_URL = "http://service1:8081"

try:
    message = requests.get("https://www.google.com").text
    data = {"hash_func": "md5", "message": message}
    response = requests.post(SERVICE1_URL, json=data)
    print(response.text)

    if response.status_code == 200:
        print("Request to service1 succeeded.")
    else:
        print(f"Request to service1 failed with status code {response.status_code}.")
except Exception as e:
    print(f"Error: {str(e)}")